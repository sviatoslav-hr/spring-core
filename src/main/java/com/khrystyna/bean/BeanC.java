package com.khrystyna.bean;

import com.khrystyna.validator.BeanValidator;
import lombok.Data;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

@Data
public class BeanC implements BeanValidator {
    private static Logger logger = LogManager.getLogger(BeanC.class);

    @Value("${beanC.name}")
    private String name;
    @Value("${beanC.value}")
    private String value;

    public BeanC() {
        logger.info(BeanC.class.getSimpleName());
    }

    public boolean validate(String s) {
        return s != null && s.length() > 0;
    }
}
