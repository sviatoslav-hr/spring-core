package com.khrystyna.bean;

import com.khrystyna.validator.BeanValidator;
import lombok.Data;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Data
public class BeanF implements BeanValidator {
    private static Logger logger = LogManager.getLogger(BeanF.class);

    private String name;
    private String value;

    public BeanF() {
        logger.info(BeanF.class.getSimpleName());
    }

    public boolean validate(String s) {
        return s != null && s.length() > 0;
    }
}
