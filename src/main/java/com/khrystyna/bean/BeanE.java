package com.khrystyna.bean;

import com.khrystyna.validator.BeanValidator;
import lombok.Data;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Data
public class BeanE implements BeanValidator {
    private static Logger logger = LogManager.getLogger(BeanE.class);

    private String name;
    private String value;

    public BeanE() {
        logger.info(BeanE.class.getSimpleName());
    }

    public boolean validate(String s) {
        return s != null && s.length() > 0;
    }
}
