package com.khrystyna.bean;

import com.khrystyna.validator.BeanValidator;
import lombok.Data;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

@Data
public class BeanD implements BeanValidator {
    private static Logger logger = LogManager.getLogger(BeanD.class);

    @Value("${beanD.name}")
    private String name;
    @Value("${beanD.value}")
    private String value;

    public BeanD() {
        logger.info(BeanD.class.getSimpleName());
    }

    public boolean validate(String s) {
        return s != null && s.length() > 0;
    }
}
