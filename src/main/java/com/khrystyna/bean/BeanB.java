package com.khrystyna.bean;

import com.khrystyna.validator.BeanValidator;
import lombok.Data;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

@Data
public class BeanB implements BeanValidator {
    private static Logger logger = LogManager.getLogger(BeanB.class);

    @Value("${beanB.name}")
    private String name;
    @Value("${beanB.value}")
    private String value;

    public BeanB() {
        logger.info(BeanB.class.getSimpleName());
    }

    public boolean validate(String s) {
        return s != null && s.length() > 0;
    }
}
