package com.khrystyna.bean;

import com.khrystyna.validator.BeanValidator;
import lombok.Data;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

@Data
public class BeanA implements BeanValidator, InitializingBean, DisposableBean {
    private static Logger logger = LogManager.getLogger(BeanA.class);

    private String name;
    private String value;

    public BeanA() {
        logger.info(BeanA.class.getSimpleName());
    }

    public boolean validate(String s) {
        return s != null && s.length() > 0;
    }

    @Override
    public void destroy() throws Exception {
        logger.info("Destroying beanA");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        logger.info("beanA afterPropertiesSet ");
    }
}
