package com.khrystyna.configuration;

import com.khrystyna.bean.BeanA;
import com.khrystyna.bean.BeanB;
import com.khrystyna.bean.BeanC;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.annotation.Order;

@Configuration
@Import(ConfigurationB.class)
@PropertySource("classpath:application.properties")
public class ConfigurationA {

    @Bean
    public BeanA getBeanA() {
        return new BeanA();
    }

    @Bean
    @Order(2)
    public BeanB getBeanB() {
        return new BeanB();
    }

    @Bean
    @Order(3)
    public BeanC getBeanC() {
        return new BeanC();
    }
}
