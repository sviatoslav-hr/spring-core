package com.khrystyna.configuration;

import com.khrystyna.bean.BeanD;
import com.khrystyna.bean.BeanE;
import com.khrystyna.bean.BeanF;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.annotation.Order;

@Configuration
public class ConfigurationB {

    @Bean
    @Order(1)
    public BeanD getBeanD() {
        return new BeanD();
    }

    @Bean
    public BeanE getBeanE() {
        return new BeanE();
    }

    @Bean
    @Lazy
    public BeanF getBeanF() {
        return new BeanF();
    }
}
