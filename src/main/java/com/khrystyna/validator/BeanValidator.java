package com.khrystyna.validator;

@FunctionalInterface
public interface BeanValidator {
    boolean validate(String s);
}
