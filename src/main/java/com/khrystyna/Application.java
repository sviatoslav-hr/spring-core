package com.khrystyna;

import com.khrystyna.bean.*;
import com.khrystyna.configuration.ConfigurationA;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {
    private static final Logger logger = LoggerFactory.getLogger(Application.class);
    /**
     * Main method that runs when the program is started.
     *
     * @param args command-line arguments
     */
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConfigurationA.class);
        BeanA beanA = context.getBean(BeanA.class);
        logger.debug(beanA.toString());
        BeanB beanB = context.getBean(BeanB.class);
        logger.debug(beanB.toString());
        BeanC beanC = context.getBean(BeanC.class);
        logger.debug(beanC.toString());
        BeanD beanD = context.getBean(BeanD.class);
        logger.debug(beanD.toString());
        BeanE beanE = context.getBean(BeanE.class);
        logger.debug(beanE.toString());
        BeanF beanF = context.getBean(BeanF.class);
        logger.debug(beanF.toString());



    }
}
